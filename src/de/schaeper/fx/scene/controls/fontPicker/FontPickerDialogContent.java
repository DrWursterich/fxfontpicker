package de.schaeper.fx.scene.controls.fontPicker;

import de.schaeper.fx.bindings.BidirectionalConverterBinding;
import de.schaeper.fx.scene.text.font.Font;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Spinner;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.util.StringConverter;

public class FontPickerDialogContent extends GridPane {
	private final ObjectProperty<Font> fontProperty;

	private final ComboBox<String> fontBox;
	private final Label previewLabel;
	private final Label preview;
	private final Insets previewInsets;
	private final Label fontLabel;
	private final Label sizeLabel;
	private final Spinner<Integer> sizeSpinner;
	private final Label styleLabel;
	private final CheckBox boldCheckBox;
	private final CheckBox italicCheckBox;
	private final HBox styleBox;

	private class SizeSpinnerFormatter extends StringConverter<Integer> {
		@Override
		public Integer fromString(final String value) {
			final Integer oldValue
					= FontPickerDialogContent.this.sizeSpinner.getValue();
			try {
				final int result = Integer.parseInt(value);
				if (result >= 1 && result <= 100) {
					return result;
				}
				throw new IllegalArgumentException("Size out of Range.");
			} catch (final Exception exception) {
				FontPickerDialogContent.this.sizeSpinner.getEditor().setText(
						oldValue.toString());
				return oldValue;
			}
		}

		@Override
		public String toString(final Integer value) {
			return value.toString();
		}
	}

	public FontPickerDialogContent(final Font font) {
		this.fontProperty = new SimpleObjectProperty<>(font);
		this.fontBox = new ComboBox<>(
				FXCollections.observableList(Font.getFamilies()));
		this.previewLabel = new Label();
		this.preview = new Label();
		this.previewInsets = new Insets(0, 1, 0, 1);
		this.fontLabel = new Label();
		this.sizeLabel = new Label();
		this.sizeSpinner = new Spinner<>(1, 100, 12);
		this.styleLabel = new Label();
		this.boldCheckBox = new CheckBox();
		this.italicCheckBox = new CheckBox();
		this.styleBox = new HBox(10);

		this.setPrefSize(200, 230);
		this.getColumnConstraints().addAll(
				new ColumnConstraints(40),
				new ColumnConstraints(160));
		this.getRowConstraints().addAll(
				new RowConstraints(20),
				new RowConstraints(35),
				new RowConstraints(35),
				new RowConstraints(35),
				new RowConstraints(30),
				new RowConstraints(30),
				new RowConstraints(30));
		this.setAlignment(Pos.CENTER);
		this.setVgap(10);
		this.setHgap(10);
		this.setPadding(new Insets(10));
		this.setBackground(new Background(new BackgroundFill(
				Color.WHITE.deriveColor(0, 1, 0.935, 1),
				new CornerRadii(5),
				this.previewInsets)));
	
		this.previewLabel.setText("Preview");
		this.preview.setText("Sample Text");
		this.preview.setMaxWidth(Double.MAX_VALUE);
		this.preview.setMaxHeight(Double.MAX_VALUE);
		this.preview.setAlignment(Pos.CENTER);
		this.preview.setBorder(new Border(new BorderStroke(
				Color.LIGHTGRAY,
				BorderStrokeStyle.SOLID,
				CornerRadii.EMPTY,
				new BorderWidths(1))));
		this.preview.setBackground(new Background(new BackgroundFill(
				Color.WHITE,
				CornerRadii.EMPTY,
				this.previewInsets)));

		this.fontLabel.setText("Font");
		this.fontBox.setMaxWidth(Double.MAX_VALUE);
		this.fontBox.setCellFactory((final ListView<String> listView) -> {
			final ListCell<String> cell = new ListCell<String>() {
				@Override
				public void updateItem(final String item, final boolean empty) {
					super.updateItem(item, empty);
					if (item != null) {
						this.setText(item);
						this.setFont(new javafx.scene.text.Font(item, 12));;
					}
				}
			};
			cell.setPrefWidth(120);
			return cell;
		});

		this.sizeLabel.setText("Size");
		this.sizeSpinner.setEditable(true);
		this.sizeSpinner.setPrefWidth(90);
		this.sizeSpinner.getValueFactory().setConverter(
				new SizeSpinnerFormatter());

		this.styleLabel.setText("Style");
		this.boldCheckBox.setText("Bold");
		this.italicCheckBox.setText("Italic");
		this.styleBox.setAlignment(Pos.CENTER_LEFT);
		this.styleBox.getChildren().addAll(
				this.boldCheckBox,
				this.italicCheckBox);

		this.add(this.previewLabel,	0, 0, 2, 1);
		this.add(this.preview,		0, 1, 2, 2);
		this.add(this.fontLabel,	0, 3, 1, 1);
		this.add(this.fontBox,		1, 3, 1, 1);
		this.add(this.sizeLabel,	0, 4, 1, 1);
		this.add(this.sizeSpinner,	1, 4, 1, 1);
		this.add(this.styleLabel,	0, 5, 1, 1);
		this.add(this.styleBox,		1, 5, 1, 1);

		this.initializeBindings();
	}

	public ObjectProperty<Font> fontProperty() {
		return this.fontProperty();
	}

	public void setFont(final Font font) {
		this.fontProperty.setValue(font);
	}

	public Font getFont() {
		return this.fontProperty.getValue();
	}

	public ObjectProperty<Font> getFontProperty() {
		return fontProperty;
	}

	public ComboBox<String> getFontBox() {
		return fontBox;
	}

	public Label getPreviewLabel() {
		return previewLabel;
	}

	public Label getPreview() {
		return preview;
	}

	public Insets getPreviewInsets() {
		return previewInsets;
	}

	public Label getFontLabel() {
		return fontLabel;
	}

	public Label getSizeLabel() {
		return sizeLabel;
	}

	public Spinner<Integer> getSizeSpinner() {
		return sizeSpinner;
	}

	public Label getStyleLabel() {
		return styleLabel;
	}

	public CheckBox getBoldCheckBox() {
		return boldCheckBox;
	}

	public CheckBox getItalicCheckBox() {
		return italicCheckBox;
	}

	public HBox getStyleBox() {
		return styleBox;
	}

	protected void initializeBindings() {
		BidirectionalConverterBinding.bind(
				this.preview.fontProperty(),
				this.fontProperty,
				Font::new,
				Font::getFxFont);
		BidirectionalConverterBinding.bind(
				this.fontBox.valueProperty(),
				this.fontProperty,
				e -> this.createFont(),
				Font::getFamily);
		BidirectionalConverterBinding.bind(
				this.boldCheckBox.selectedProperty(),
				this.fontProperty,
				e -> this.createFont(),
				Font::isBold);
		BidirectionalConverterBinding.bind(
				this.italicCheckBox.selectedProperty(),
				this.fontProperty,
				e -> this.createFont(),
				Font::isItalic);
		BidirectionalConverterBinding.bind(
				this.sizeSpinner.getValueFactory().valueProperty(),
				this.fontProperty,
				e -> this.createFont(),
				e -> (int)e.getSize());
	}

	private Font createFont() {
		return new Font(
				this.fontBox.getValue(),
				this.boldCheckBox.isSelected()
						? FontWeight.BOLD
						: FontWeight.NORMAL,
				this.italicCheckBox.isSelected()
						? FontPosture.ITALIC
						: FontPosture.REGULAR,
				this.sizeSpinner.getValue());
	}
}
