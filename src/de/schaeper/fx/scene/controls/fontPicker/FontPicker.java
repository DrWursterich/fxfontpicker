package de.schaeper.fx.scene.controls.fontPicker;

import java.util.function.Supplier;

import de.schaeper.fx.scene.text.font.Font;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.Event;
import javafx.scene.control.ComboBoxBase;
import javafx.scene.control.Dialog;
import javafx.scene.control.Skin;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;


public class FontPicker extends ComboBoxBase<Font> {
	private final Tooltip tooltip;
	private FontPickerSkin skin;
	private ObjectProperty<Supplier<Dialog<Font>>> dialogFactoryProperty;

	public FontPicker(final Font font) {
		super();
		this.tooltip = new Tooltip();
		this.dialogFactoryProperty = new SimpleObjectProperty<>(
				() -> new FontPickerDialog(this.getValue()));
		this.skin = new FontPickerSkin(this);
		this.setPrefWidth(133);

		this.setOnMouseClicked(this::openDialog);
		this.setOnAction(this::openDialog);
		this.setOnKeyPressed(this::openDialog);

		Tooltip.install(this, this.tooltip);

		this.skinProperty().addListener((v, o, n) -> {
			if (n == null) {
				this.skin = null;
			} else if (n instanceof FontPickerSkin) {
				this.skin = (FontPickerSkin)n;
			} else {
				this.setSkin(this.skin);
			}
		});

		this.valueProperty().addListener((v, o, n) -> {
			this.skin.updateFont();
			this.updateTooltip();
		});
		this.setValue(font);
	}

	private void updateTooltip() {
		final Font font = this.getValue();
		this.tooltip.setText(font.toString());
		this.tooltip.setFont(font.getFxFont());
	}

	private void openDialog(final Event event) {
		if ((event instanceof KeyEvent)
				&& !this.isValidKeyEvent((KeyEvent)event)) {
			return;
		}
		if ((event instanceof MouseEvent)
				&& !this.isValidMouseEvent((MouseEvent)event)) {
			return;
		}
		event.consume();
		this.requestFocus();
		final Dialog<Font> dialog = this.dialogFactoryProperty.getValue().get();
		final Font result = dialog.showAndWait().orElse(null);
		if (result != null) {
			this.setValue(result);
		}
	}

	private boolean isValidKeyEvent(final KeyEvent keyEvent) {
		final KeyCode keyCode = keyEvent.getCode();
		return ((KeyCode.ENTER.equals(keyCode)
					|| KeyCode.SPACE.equals(keyCode))
				&& keyEvent.getTarget().equals(this)
				&& !(keyEvent.isConsumed()
					|| keyEvent.isAltDown()
					|| keyEvent.isMetaDown()
					|| keyEvent.isControlDown()
					|| keyEvent.isShiftDown()
					|| keyEvent.isShortcutDown()));
	}

	private boolean isValidMouseEvent(final MouseEvent mouseEvent) {
		return (MouseButton.PRIMARY.equals(mouseEvent.getButton())
				&& (mouseEvent.getTarget().equals(this)
					|| this.getChildren().contains(mouseEvent.getTarget()))
				&& !(mouseEvent.isConsumed()
					|| mouseEvent.isAltDown()
					|| mouseEvent.isMetaDown()
					|| mouseEvent.isControlDown()
					|| mouseEvent.isShiftDown()
					|| mouseEvent.isShortcutDown()));
	}

	@Override
	public Skin<?> createDefaultSkin() {
		return this.skin;
	}

	public Supplier<Dialog<Font>> getDialogFactory() {
		return this.dialogFactoryProperty.getValue();
	}

	public void setDialogFactory(final Supplier<Dialog<Font>> dialogFactory) {
		this.dialogFactoryProperty.setValue(dialogFactory);
	}

	public ObjectProperty<Supplier<Dialog<Font>>> dialogFactoryProperty() {
		return this.dialogFactoryProperty;
	}
}
