package de.schaeper.fx.scene.controls.fontPicker;

import de.schaeper.fx.scene.text.font.Font;

import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

public class FontPickerDialog extends Dialog<Font> {
	private final FontPickerDialogContent content;

	public FontPickerDialog(final Font defaultFont) {
		this.content = new FontPickerDialogContent(defaultFont);
		this.setResultConverter(
				button -> button == ButtonType.OK
					? this.content.getFont()
					: null);
		this.setTitle("FontPicker-Dialog");
		this.getDialogPane().setHeaderText("Select a Font");
		this.getDialogPane().getButtonTypes().addAll(
				ButtonType.OK,
				ButtonType.CANCEL);
		this.getDialogPane().setContent(this.content);
	}
}
