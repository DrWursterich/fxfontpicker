package de.schaeper.fx.scene.controls.fontPicker;

import de.schaeper.fx.scene.text.font.Font;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ComboBoxBase;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.skin.ComboBoxPopupControl;
import javafx.util.StringConverter;

public class FontPickerSkin extends ComboBoxPopupControl<Font> {
	private final Label displayNode;

	public FontPickerSkin(final ComboBoxBase<Font> comboBoxBase) {
		super(comboBoxBase);

		this.displayNode = new Label();
		this.displayNode.setManaged(false);
		this.displayNode.setPadding(new Insets(4, 4, 4, 8));
		this.displayNode.setOnMouseClicked(comboBoxBase::fireEvent);

		if (comboBoxBase.isShowing()) {
			this.show();
		}
	}

	@Override
	protected StringConverter<Font> getConverter() {
		return null;
	}

	@Override
	protected TextField getEditor() {
		return null;
	}

	@Override
	protected Node getPopupContent() {
		return null;
	}

	@Override
	public Node getDisplayNode() {
		return this.displayNode;
	}

	public void updateFont() {
		final Font font = this.getSkinnable().getValue();
		this.displayNode.setText(font.toString());
	}
}
