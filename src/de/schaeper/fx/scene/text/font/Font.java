package de.schaeper.fx.scene.text.font;

import java.util.List;
import java.util.Objects;

import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;


public class Font {

	private static final Font DEFAULT = new Font(
			javafx.scene.text.Font.getDefault());

	public static Font getDefault() {
		return Font.DEFAULT;
	}

	public static List<String> getFamilies() {
		return javafx.scene.text.Font.getFamilies();
	}

	private final String family;
	private final FontWeight weight;
	private final FontPosture posture;
	private final double size;
	private final javafx.scene.text.Font font;

	public Font(
			final String family,
			final FontWeight weight,
			final FontPosture posture,
			final double size) {
		this.family = family;
		this.weight = weight;
		this.posture = posture;
		this.size = size;
		this.font = javafx.scene.text.Font.font(
				this.family,
				this.weight,
				this.posture,
				this.size);
	}

	public Font(final String family, final double size) {
		this(family, FontWeight.NORMAL, FontPosture.REGULAR, size);
	}

	public Font(final javafx.scene.text.Font fxFont) {
		this(fxFont.getFamily(), fxFont.getSize());
	}

	public String getName() {
		return this.font.getName();
	}

	public String getFamily() {
		return this.family;
	}

	public FontWeight getFontWeight() {
		return this.weight;
	}

	public FontPosture getFontPosture() {
		return this.posture;
	}

	public double getSize() {
		return this.size;
	}

	public javafx.scene.text.Font getFxFont() {
		return this.font;
	}

	public boolean isBold() {
		switch (this.weight) {
			case EXTRA_BOLD:
			case BOLD:
			case SEMI_BOLD:
			case BLACK:
				return true;
			case EXTRA_LIGHT:
			case LIGHT:
			case THIN:
			case MEDIUM:
			case NORMAL:
			default:
				return false;
		}
	}

	public boolean isItalic() {
		switch (this.posture) {
			case ITALIC:
				return true;
			case REGULAR:
			default:
				return false;
		}
	}

	@Override
	public String toString() {
		return String.format("%s, %.0f", this.family, this.size);
	}

	@Override
	public boolean equals(final Object other) {
		if (other instanceof Font) {
			final Font that = (Font)other;
			return this.getFamily().equals(that.getFamily())
					&& this.getName().equals(that.getName())
					&& this.getFontWeight().equals(that.getFontWeight())
					&& this.getFontPosture().equals(that.getFontPosture())
					&& this.getSize() == that.getSize();
		}
		if (other instanceof javafx.scene.text.Font) {
			return this.font.equals(other);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.getFamily(),
				this.getName(),
				this.getFontWeight(),
				this.getFontPosture(),
				this.getSize());
	}
}
