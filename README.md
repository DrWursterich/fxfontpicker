# FxFontPicker

The FxFontPicker is a Component for JavaFX 11, allowing simple Font-Customizations.

## Installing

### Using Jam

Add the following lines to your `jam.ini`:
```ini
[fxfontpicker]
url=https://gitlab.com/DrWursterich/fxfontpicker
version=1.0.1
```

You can then run `jam build` as usual.
For more details see [jam](https://gitlab.com/DrWursterich/jam).

### Manually

You will also have to install the Dependencies manually, namely:
* [FxBindings](https://gitlab.com/DrWursterich/fxbindings)
* [Conditionals](https://gitlab.com/DrWursterich/conditionals)

```bash
git clone https://gitlab.com/DrWursterich/conditionals.git
git clone https://gitlab.com/DrWursterich/fxbindings.git
git clone https://gitlab.com/DrWursterich/fxfontpicker.git
cd fxfontpicker
javac $(find src -name *.java) --module-path /path/to/javafx-sdk --add-modules javafx.base,javafx.scene,javafx.control -cp src:../fxbindings/src:../conditionals/src
jar -cvf fxfontpicker.jar src
mv fxfontpicker.jar /my/project/lib/
```

## Examples

```java
public static void main(final String...args) {
	Application.launch(args);
}

public void start(final Stage primaryStage) {
	final StackPane root = new StackPane();
	root.setPadding(new Insets(20));

	final FontPicker = new FontPicker(Font.getDefault());
	root.add(fontPicker);

	primaryStage.setScene(new Scene(root, 200, 200));
	primaryStage.show();
}
```

![Example Application](resources/ExampleApplication.png)

![Example Dialog](resources/ExampleDialog.png)

